Linux Kernel Hacking: A Crash Course
================================

This repo is just a simple code system that follows the talk given by Georgi Knox on hacking
the linux kernel.  

**NOTE:** This is not my code but it from her github repo that is linked below.  (Want to make sure
to give credit where credit it due... and it was a fantastic talk)  

[Talk - Infoq](https://www.infoq.com/presentations/linux-kernel/?utm_source=infoqEmail&utm_medium=WeeklyNL_EditorialContentDevelopment&utm_campaign=10202015news)  
[Github repo](https://github.com/georgicodes/linux)  

## Build/Run hi

1. Navigate to `hi-kernel` directory
2. Build using make

        make

3. Load using insmod

        insmod hi.ko

4. Look at output

        dmesg

5. List using lsmod

        lsmod | grep hi

6. Unload using rmmod

        rmmod hi

